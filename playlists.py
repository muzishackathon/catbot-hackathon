import psycopg2
import psycopg2.extras
import json

def get_playlist(chat_id):
    conn = connection()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute('SELECT * FROM playlists WHERE chat_id = %s', (chat_id,))
    playlist = cursor.fetchone()
    playlist = {'chat_id': playlist['chat_id'], 'playlist': json.loads(playlist['playlist'])}

    return playlist

def create_playlist(chat_id, playlist):
    conn = connection()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    try:
        cursor.execute('INSERT INTO playlists VALUES(%s,%s)', (chat_id, playlist.reset_index().to_json(orient = 'records')))
    except Exception, e:
        pass

def update_playlist(chat_id, playlist):
    conn = connection()
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    cursor.execute('UPDATE playlists SET playlist = %s WHERE chat_id = %s', (playlist, chat_id))

def connection():
    conn = psycopg2.connect("dbname=catbot user=postgres password=postgres")
    conn.autocommit = True

    return conn

# CREATE TABLE playlists (chat_id integer, playlist text)