import ApplicationService as aps
import logging

def main():
    aps.logger = logging.getLogger(__package__)
    aps.init()
    aps.run()

if __name__ == ('__main__'):
    main()