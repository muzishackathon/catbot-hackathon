import re
from collections import OrderedDict
import telepot
import numpy as np

import playlists
from Answers import Answers
from Commands import Commands, MuzisCommands
from MessageModel import Message, Track
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton
from MuzisAPI import MuzisAPI
from bot_rank import Bot_Rank
import pandas
import os.path as pth

catigoriesFile = pth.join(pth.dirname(pth.abspath(__file__)),'filedValues.csv')

class PartyBot(object):

    def __init__(self, tkn):
        self._tkn = tkn
        self.bot = telepot.Bot(tkn)
        self.bot.message_loop(self.handle)
        self.chatsPlaylist = {}
        self._muzis = MuzisAPI()
        self._rangeModel = Bot_Rank()
        self._catigories = self.loadCatigories(catigoriesFile)
        self._threashold = 3

    def handle(self,msg):
        print(msg)
        content_type, chat_type, chat_id = telepot.glance(msg)
        userID = np.long(msg[Message.sender.value][Message.senderID.value])
        userName = msg[Message.sender.value][Message.sender_first_name.value]
        response = self.buildAnswer(chat_id,userName, msg[Message.text.value],msg.get(Message.reply_to_message.value))
        self.respond(chat_id,response)

    def respond(self,toUser,message):
        if 'ok' not in message:
            self.bot.sendMessage(toUser, message)

    def buildAnswer(self, chatID, userName, messageText,replyToMsg):
        response = 'ok'
        if chatID and 'start' not in messageText:
            if self.chatsPlaylist.get(chatID) is None:
                self.chatsPlaylist[chatID] = pandas.DataFrame(columns=[Track.id.value,Track.track_name.value,
                                                                       Track.performer_id.value,Track.genre.value,
                                                                       Track.creationTime.value,Track.lang.value,
                                                                       Track.pace.value,Track.theme.value,
                                                                       Track.like.value,Track.order.value,
                                                                       Track.performer.value,
                                                                       Track.file_mp3.value]).set_index(Track.id.value)
                playlists.create_playlist(chatID,self.chatsPlaylist[chatID])

            print('search track %s'%messageText)
            if Commands.addTrack.value in messageText:
                command = self.parseCommand(messageText,Commands.addTrack.value)
                print(command)
                foundTracks = self._muzis.search({MuzisCommands.q_track.value:command.values()[0]})
                if len(foundTracks)>0 and foundTracks.get(MuzisCommands.songs.value):
                    track = foundTracks[MuzisCommands.songs.value][0]
                    # print(track)
                    if track[Track.id.value]:
                        response = Answers.verifyIsThisTrack.value%(userName,track[Track.track_name.value],
                                                                    track[Track.performer.value])
                        # print('rewrite ')
                        # self.chatsPlaylist[chatID] = self.chatsPlaylist[chatID].append(self.buildTrack(track))
                        # response = Answers.trackWasAdded.value%(userName)
                else:
                    response = Answers.noTrackWithSuchName.value%(userName)
            elif Commands.approve.value in messageText:
                if replyToMsg is not None:
                    command = self.parseCommand(messageText,Commands.approve.value)
                    print(command)
                    trackName = re.search('%s\s*\w*'%('track:'),replyToMsg[Message.text.value])
                    if trackName is not None:
                        trackName = trackName.group(0).replace('track: ','')
                        print('track name %s'%(trackName))
                        foundTracks = self._muzis.search({MuzisCommands.q_track.value:trackName})
                        if len(foundTracks)>0 and foundTracks.get(MuzisCommands.songs.value):
                            track = foundTracks[MuzisCommands.songs.value][0]
                            # print(track)
                            if track[Track.id.value]:
                                playlist = playlists.get_playlist(chatID)['playlist']
                                if playlist is not None:
                                    self.chatsPlaylist[chatID] = pandas.DataFrame(playlist)
                                    if self.chatsPlaylist[chatID].empty:
                                        self.chatsPlaylist[chatID] = pandas.DataFrame(columns=[Track.id.value,Track.track_name.value,
                                                                               Track.performer_id.value,Track.genre.value,
                                                                               Track.creationTime.value,Track.lang.value,
                                                                               Track.pace.value,Track.theme.value,
                                                                               Track.like.value,Track.order.value,
                                                                               Track.performer.value,
                                                                               Track.file_mp3.value]).set_index(Track.id.value)
                                    else:
                                        self.chatsPlaylist[chatID] = self.chatsPlaylist[chatID].set_index(Track.id.value)
                                self.chatsPlaylist[chatID] = self.chatsPlaylist[chatID].append(self.buildTrack(track)).fillna(0,axis=1)
                                self.rankAndStorePlaylist(chatID)
                                response = Answers.trackWasAdded.value%(userName,track[Track.track_name.value])
                        else:
                            response = Answers.noTrackWithSuchName.value%(userName)
                    else:
                        response = Answers.provideTrackName.value%(userName)
                else:
                    response = Answers.pleaseApproveReply.value%(userName)
            elif Commands.list.value in messageText:
                # need from base
                playlist = playlists.get_playlist(chatID)['playlist']
                if playlist is not None:
                    print pandas.DataFrame(playlist)
                    self.chatsPlaylist[chatID] = pandas.DataFrame(playlist)
                    if self.chatsPlaylist[chatID].empty:
                        self.chatsPlaylist[chatID] = pandas.DataFrame(columns=[Track.id.value,Track.track_name.value,
                                              Track.performer_id.value,Track.genre.value,
                                              Track.creationTime.value,Track.lang.value,
                                              Track.pace.value,Track.theme.value,
                                              Track.like.value,Track.order.value,
                                              Track.performer.value,
                                              Track.file_mp3.value]).set_index(Track.id.value)
                    else:
                        self.chatsPlaylist[chatID] = self.chatsPlaylist[chatID].set_index(Track.id.value)
                response = str(self.chatsPlaylist[chatID][[Track.performer.value, Track.track_name.value, Track.order.value]])
            elif Commands.player.value in messageText:
                response = 'http://127.0.0.1:3000/index.html?n='+str(chatID)
            else:
                response = self.likeChecker(chatID,messageText,userName)
        return response

    def parseCommand(self,str,match):
        dictRes = {}
        if str and match:
            print (str,match)
            res = re.search('%s\s*.*'%(match),str)
            print (res)
            if res:
                strSet = res.group(0).replace(match,'')
                if '=' in strSet:
                    kv = strSet.split('=')
                    dictRes[kv[0]]=kv[1]
                else:
                    dictRes[MuzisCommands.song.value]=strSet.strip()
        return dictRes

    def rangeModel(self, playlist):
        if playlist is not None:

            return self._rangeModel.predict(playlist.values)
        else:
            np.array([])

    def loadCatigories(self, fileName):
        try:
            dataFrame = pandas.read_csv(fileName, index_col=0, parse_dates=True, dayfirst=True, header=None)
        except Exception,e:
            dataFrame = pandas.DataFrame()
        return dataFrame

    def buildTrack(self,track):
        valsMap = {Track.id.value:track[Track.id.value],
                   Track.performer_id.value:track[Track.performer_id.value],
                   Track.track_name.value:track[Track.track_name.value],
                   Track.like.value:0,
                   Track.performer.value:track[Track.performer.value],
                   Track.file_mp3.value:track[Track.file_mp3.value]}
        valuesVec = track[Track.values_all.value]
        if len(valuesVec)>0:
            for elem in valuesVec:
                valsMap[self._catigories.loc[elem][2]] = elem
        trackDF = pandas.DataFrame(valsMap, index=[0]).set_index(Track.id.value)
        print(trackDF)
        return trackDF
    
    def likeChecker(self, chatId, messageText,userName):
        trackName = ''
        value = 0
        if Commands.dislike.value in messageText:
            command = self.parseCommand(messageText, Commands.dislike.value)
            trackName = command.values()[0].strip()
            print trackName
            fileterdSet = self.chatsPlaylist[chatId][self.chatsPlaylist[chatId][Track.track_name.value].str.strip()
                                                     == trackName]
            if fileterdSet.shape[0]>0:
                value = self.chatsPlaylist[chatId].loc[fileterdSet.index[0]][Track.like.value]
                value-=1
                self.chatsPlaylist[chatId].set_value(fileterdSet.index[0],Track.like.value,value)
            else:
                return Answers.likedTrackNotInPlaylist.value%(userName,trackName)
        elif Commands.like.value in messageText:
            command = self.parseCommand(messageText, Commands.like.value)
            trackName = command.values()[0].strip()
            print trackName,self.chatsPlaylist[chatId]
            fileterdSet = self.chatsPlaylist[chatId][self.chatsPlaylist[chatId][Track.track_name.value].str.strip()
                                                     == trackName]
            if fileterdSet.shape[0]>0:
                print self.chatsPlaylist[chatId].loc[fileterdSet.index[0]]
                value = self.chatsPlaylist[chatId].loc[fileterdSet.index[0]][Track.like.value]
                value+=1
                self.chatsPlaylist[chatId].set_value(fileterdSet.index[0],Track.like.value,value)
            else:
                return Answers.likedTrackNotInPlaylist.value%(userName,trackName)

        self.rankAndStorePlaylist(chatId)
        # print('show filtered data %s'%self.chatsPlaylist[chatId])
        return Answers.currentVotes.value%(userName,trackName,value)

    def rankAndStorePlaylist(self,chatID):
        if self.chatsPlaylist[chatID].shape[0]>self._threashold:
            print(self.chatsPlaylist[chatID])
            self.chatsPlaylist[chatID].sort_values([Track.like.value], ascending=False)
            print self.chatsPlaylist[chatID]
            '''
            rangelist = self.rangeModel(self.chatsPlaylist[chatID].
                                        drop([Track.track_name.value,Track.order.value,
                                              Track.performer.value,Track.file_mp3.value,Track.creationTime.value,
                                              Track.lang.value,Track.pace.value,Track.theme.value,
                                              Track.performer_id.value,Track.genre.value],axis=1))
            print('here was returned list %s'%(rangelist))
            self.chatsPlaylist[chatID][Track.order.value] = rangelist
            '''
        playlists.update_playlist(chatID,self.chatsPlaylist[chatID].reset_index().to_json(orient = 'records'))