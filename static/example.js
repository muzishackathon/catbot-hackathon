var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var Player = React.createClass({
    getInitialState: function () {
        return {data: {
            playlist: []
        }
        };
    },
    loadData: function () {
        $.ajax({
            url: this.props.url + getUrlParameter("n"),
            dataType: 'json',
            cache: false,
            success: function (data) {
                console.log(data);
                this.setState({data: data});
            }.bind(this),
            error: function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    componentDidMount: function () {
        this.loadData()
    },
    render: function () {
        return (
            <div className="player">
                <Current load={this.loadData} data={this.state.data.playlist[0]}/>
                <Candidates data={this.state.data.playlist.slice(1)}/>
            </div>
        );
    }
});

var Candidates = React.createClass({
    render: function() {
        console.log(this.props.data)
        var nodes = this.props.data.map(function(c) {
            var name = c.performer
            var track = c.track_name
            return (
                <p>{name} - {track}</p>
            );
        });
        return(
        <div className="candidates">
            {nodes}
        </div>);
    }
})

var Current = React.createClass({
    handleEnd: function (e) {
        location.reload();
    },
    componentDidMount: function () {
        setTimeout(function () {
            $("#pl")[0].play()
        }, 100)
    },
    render: function () {
        if (this.props.data) {
            var mp3 = "http://f.muzis.ru/" + this.props.data.file_mp3;
            var name = this.props.data.performer
            var track = this.props.data.track_name
            return (
                <div className="currentPlay">
                    <p>{name} - {track}</p>
                    <audio id="pl" controls autoplay="true" onEnded={this.handleEnd} src={mp3}>
                    </audio>
                </div>
            );
        } else {
            return (
                <div className="currentPlay">
                    Empty :(
                </div>
            )
        }
    }
});

ReactDOM.render(
    <Player url="/get_next_track/"/>,
    // <NewPlay/>,
    document.getElementById('content')
);
