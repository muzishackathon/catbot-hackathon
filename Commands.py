from enum import Enum


class Commands(Enum):
    addTrack = 'add'
    suggest = 'suggest'
    approve = 'ok'
    like = 'like'
    dislike = 'dislike'
    list = 'list'
    player = 'player'

class MuzisCommands(Enum):
    q_track = 'q_track'
    q_performer = 'q_performer'
    songs = 'songs'
    song = 'song'