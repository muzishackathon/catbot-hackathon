from enum import Enum

class Answers(Enum):
    helloUser = 'Hello here is PartyMaker, I can help you to make a playlist for your party'
    provideTrackName = '%s, could you please provide track name, that must be added to playlist?'
    noTrackWithSuchName = '%s, there was no such track, could you please provide another one?'
    trackWasAdded = '%s, requested track: "%s" was added.'
    verifyIsThisTrack = '%s, track: %s of performer: %s?'
    pleaseApproveReply = '%s, please approve the track suggestion as reply.'
    likedTrackNotInPlaylist = '%s, the track "%s" you have not in playlist'
    currentVotes = '%s, you voice was accepted, current rating of track "%s" is %s'
